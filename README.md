# CS373: Software Engineering Collatz Repo

* Name: William Li

* EID: wzl62

* GitLab ID: williamli1

* HackerRank ID: williamli1

* Git SHA: 6eb675bf83c816f0ff3d1735b71949df33ea8ba7

* GitLab Pipelines: https://gitlab.com/williamli1/cs373-collatz/pipelines

* Estimated completion time: 5 hrs

* Actual completion time: 10 hrs
