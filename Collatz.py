#!/usr/bin/env python3

# ----------
# Collatz.py
# ----------

# pylint: disable = bad-whitespace
# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# -------
# imports
# -------

from typing import IO, List

# ------------
# collatz_read
# ------------


def collatz_read(s: str) -> List[int]:
    """
    read two ints
    s a string
    return a list of two ints, representing the beginning and end of a range, [i, j]
    """
    a = s.split()
    return [int(a[0]), int(a[1])]


# ------------
# collatz_eval
# ------------

# lazy cache

cache = dict()
cache[1] = 1


def collatz_eval(i: int, j: int) -> int:
    """
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    return the max cycle length of the range [i, j]
    """

    # check pre-conditions

    assert i > 0
    assert j > 0

    # check ranges

    low = i
    high = j

    if i > j:
        low = j
        high = i
    elif i < j // 2:
        low = j // 2
        high = j

    max_cycle = 0
    for x in reversed(range(low, high + 1)):

        # only calculate if not in cache

        cycle = cache[x] if x in cache else collatz_helper(x)

        if cycle > max_cycle:
            max_cycle = cycle

    # check post-conditions
    assert max_cycle > 0

    return max_cycle


# ------------
# collatz_helper
# ------------


def collatz_helper(x: int) -> int:
    """
    x original number
    return collatz cycle length for x
    """

    index = x
    cycle = 0

    while x not in cache:
        if x % 2 == 0:
            x = x // 2
            cycle += 1
        else:

            # odd number optimization

            x += (x >> 1) + 1
            cycle += 2

    cycle += cache[x]
    cache[index] = cycle

    return cycle


def collatz_print(w: IO[str], i: int, j: int, v: int) -> None:
    """
    print three ints
    w a writer
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    v the max cycle length
    """
    w.write(str(i) + " " + str(j) + " " + str(v) + "\n")


# -------------
# collatz_solve
# -------------


def collatz_solve(r: IO[str], w: IO[str]) -> None:
    """
    r a reader
    w a writer
    """
    for s in r:
        i, j = collatz_read(s)
        v = collatz_eval(i, j)
        collatz_print(w, i, j, v)
